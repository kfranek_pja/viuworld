(let getMapField (map x y) {
    (let line (Std.Vector.at map y))
    (Std.String.at line x)
})

(let sendCommand (command x y name world) {
    (let msg (struct))
    (:= msg.command command)
    (:= msg.x x)
    (:= msg.y y)
    (:= msg.addr (Std.Actor.self))
    (:= msg.name name)
    (Std.Actor.send world msg)
    (let answer (Std.Actor.receive))
    answer
})

;(let astarFindReward (world name queue i) {
;    (let current (Std.Vector.at_copy queue i))
;    (let x (Std.copy current.x))
;    (let y (Std.copy current.y))

;    (let l (sendCommand "check" (+ x 1) y name world))
;    (let r (sendCommand "check" (- x 1) y name world))
;    (let t (sendCommand "check" x (+ y 1) name world))
;    (let b (sendCommand "check" x (- y 1) name world))
;
;    (checkForEnq l (+ x 1) y queue)
;    (checkForEnq r (- x 1) y queue)
;    (checkForEnq t x (+ y 1))
;    (checkForEnq b x (- y 1))
;})



(let astarPony (world name) {
    ;(print "Astar was born!")
    ;(print (Std.String.concat "His name is " name))

    ;(astarFindTheReward world name 0 0)
    (sendCommand "move" 0 -1 name world)
    (sendCommand "move" 0 -1 name world)
    (sendCommand "move" 0 -1 name world)
    0
})


(let dijksPony (world name) {
    ;(print "Dijkstra was born!")
    ;(print (Std.String.concat "His name is " name))
    (sendCommand "move" 1 0 name world)
    (sendCommand "move" -1 0 name world)
    (sendCommand "move" 0 -1 name world)
    0
})

(let compareNextLetter (alphabet char i n) {
        (if 
            (Std.String.eq (Std.String.at alphabet i) char)
            (< i n) ;here could be just "true", but asm parser is just a fucked up bitch :v
            (if 
                (< (+ i 1) n)
                (tailcall compareNextLetter alphabet char (+ i 1) n)
                false
            )
        )
})

(let isLetter (char) {
    (let alphabet "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
    (compareNextLetter alphabet char 0 (Std.String.size alphabet))
})

(let addPony (foundPonies name x y) {
    (let newPony (struct))
    (:= newPony.name name)
    (:= newPony.x x)
    (:= newPony.y y)
    (:= newPony.score 0)
    (Std.Vector.push foundPonies newPony)
    foundPonies
})

(let addReward (foundRewards x y) {
    (let newReward (struct))
    (:= newReward.x x)
    (:= newReward.y y)
    (:= newReward.present 1)
    (Std.Vector.push foundRewards newReward)
    foundRewards
})



(let extractPoniesInField (map x y w h foundPonies) {
    (let field (getMapField map x y))
    (if
        (isLetter field)
        (let foundPonies (addPony foundPonies field x y))
        (let foundPonies foundPonies)
    )
    (if
        (< (+ x 1) w)
        (tailcall extractPoniesInField map (+ x 1) y w h foundPonies)
        (if
            (< (+ y 1) h)
            (tailcall extractPoniesInField map 0 (+ y 1) w h foundPonies)
            foundPonies
        )
    )
})

(let countRewardsInField (map x y w h rewardCounter) {
    (let field (getMapField map x y))
    (if
        (Std.String.eq field "*")
        (let rewardCounter (+ rewardCounter 1))
        (let rewardCounter rewardCounter)
    )
    (if
        (< (+ x 1) w)
        (tailcall countRewardsInField map (+ x 1) y w h rewardCounter)
        (if
            (< (+ y 1) h)
            (tailcall countRewardsInField map 0 (+ y 1) w h rewardCounter)
            rewardCounter
        )
    )
})

(let extractRewardsInField (map x y w h rewards) {
    (let field (getMapField map x y))
    (if
        (Std.String.eq field "*")
        (let rewards (addReward rewards x y))
        (let rewards rewards)
    )
    (if
        (< (+ x 1) w)
        {(tailcall extractRewardsInField map (+ x 1) y w h rewards)}
        (if
            (< (+ y 1) h)
            (tailcall extractRewardsInField map 0 (+ y 1) w h rewards)
            rewards
        )
    )
})


(let findAppliedStrategy (name strategies i) {
    (let strategy (Std.Vector.at_copy strategies i))
    (let locName name)
    (if
        (Std.String.eq (Std.String.substr strategy 0 1) name)
        (Std.String.substr strategy 2 7)
        (if
            (< (+ i 1) (Std.Vector.size strategies))
            (tailcall findAppliedStrategy name strategies (+ i 1))
            "astar"
        )
    ) 
})

(let findPonyByName (ponies name i) {
    (let pony (Std.Vector.at_copy ponies i))
    (if 
        (Std.String.eq (Std.copy pony.name) name)
        {           
            ;(let name name)
            i
        }
        (if 
            (< (+ i 1) (Std.Vector.size ponies))
            (tailcall findPonyByName ponies name (+ i 1))
            -1
        )
    )
})

(let findPonyByPosition (ponies x y i) {
    (let pony (Std.Vector.at_copy ponies i))
    (if 
        (and
            (= (Std.copy pony.x) x)
            (= (Std.copy pony.y) y)
        )
        i
        (if 
            (< (+ i 1) (Std.Vector.size ponies))
            (tailcall findPonyByPosition ponies x y (+ i 1))
            -1
        )
    )
})

(let findRewardByPosition (rewards x y i) {
    (let reward (Std.Vector.at_copy rewards i))
    (if 
        (and
            (= (Std.copy reward.x) x)
            (= (Std.copy reward.y) y)
        )
        (Std.copy reward.present)
        (if 
            (< (+ i 1) (Std.Vector.size rewards))
            (tailcall findRewardByPosition rewards x y (+ i 1))
            0
        )
    )
})

(let spawnPonyActor (strategy name world) {
    ;(print (Std.String.concat "Spawnujemy " strategy))
    (if 
        (Std.String.eq strategy "astar")
        {
            ;(print "Running astar") 
            (actor astarPony world name)
        }
        (if
            (Std.String.eq strategy "dijks")
            {
                ;(print "Running dijkstra") 
                (actor dijksPony world name)
            }
            {
                ;(print "Nothing to run")
                (actor dijksPony world name)
            }
        )
    )
})

(let runPony (ponies appliedStrategies world i) {
    (let pony (Std.Vector.at_copy ponies i))
    (let strategy (findAppliedStrategy (Std.copy pony.name) appliedStrategies 0))
    (spawnPonyActor strategy (Std.copy pony.name) world)
    (if
        (< (+ i 1) (Std.Vector.size ponies))
        (tailcall runPony ponies appliedStrategies world (+ i 1))
        ponies
    )
})

(let updatePonyField (ponies updPonies ponyIndex x y reward i){
    (let pony (Std.Vector.at_copy ponies i))
    (if
        (= i ponyIndex)
        {
            (:= pony.x (+ (Std.copy pony.x) x))
            (:= pony.y (+ (Std.copy pony.y) y))
            (if
                (= reward 1)
                (:= pony.score (+ (Std.copy pony.score) 1))
                (let pony pony)
            )
        }
        {
            (let pony pony)
        }
    )
    (Std.Vector.push updPonies pony)
    (if
        (< (+ i 1) (Std.Vector.size ponies))
        (updatePonyField ponies updPonies ponyIndex x y reward (+ i 1))
        updPonies
    )
})


(let findForTakenReward (rewards updRewards x y i){
    (let reward (Std.Vector.at_copy rewards i))
    (if
        (and
            (= (Std.copy reward.x) x)
            (= (Std.copy reward.y) y)        
        )
        {
            
            (:= reward.present 0)
        }
        {
            (let x x)
        }
    )
    (Std.Vector.push updRewards reward)
    (if
        (< (+ i 1) (Std.Vector.size rewards))
        (findForTakenReward rewards updRewards x y (+ i 1))
        updRewards
    )
})


(let takeReward (rewards ponies ponyIndex x y) {
    (let pony (Std.Vector.at_copy ponies ponyIndex))
    (let x (+ (Std.copy pony.x) x))
    (let y (+ (Std.copy pony.y) y))
    (findForTakenReward rewards (vector) x y 0)
})

(let getPonyNameByIndex (ponies index) {
    (let pony (Std.Vector.at_copy ponies index))
    (Std.copy pony.name)
})

(let updateTheMapLine (line map w x y ponies rewards builtLine) {
    (let field (Std.String.substr line x (+ x 1)))
    (if 
        (> 
            (findPonyByPosition ponies x y 0)
            -1
        )
        (let field (getPonyNameByIndex ponies (findPonyByPosition ponies x y 0)))
        {
            
            (if
                (Std.String.eq field "*")
                (if
                    (=                                
                        (findRewardByPosition rewards x y 0)
                        1
                    )
                    (let field "*")
                    (let field " ")
                )
                {
                    
                    (if
                        (Std.String.eq field "#")
                        (let field "#")
                        (let field " ")
                    )
                }
            )
        }
    )
    

    (let builtLine (Std.String.concat builtLine field))

    (if 
        (<
            (+ x 1)
            w
        )
        (updateTheMapLine line map w (+ x 1) y ponies rewards builtLine)
        (Std.String.concat builtLine "|")
    )    
})

(let printOutTheWorld (map w h y ponies rewards) {
    (let line (Std.Vector.at_copy map y))
    (print (updateTheMapLine line map w 0 y ponies rewards ""))
    
    (if 
        (<
            (+ y 1)
            h
        )
        (printOutTheWorld map w h (+ y 1) ponies rewards)
        {
            (print "=================")
            0
        }
    )  
})

(let checkFieldStatus (msg map w h ponies rewardCounter rewards ponyIndex) {
    ;(print "========")
    ;(print msg)
    ;(print map)
    ;(print w)
    ;(print h)
    ;(print ponies)
    ;(print rewardCounter)
    ;(print rewards)
    ;(print ponyIndex)
    ;(print "========")
    (let rewardCounter (+ rewardCounter 0)) ;cause opt
    (let rewards (Std.copy rewards))        ;cause opt

    (let pony (Std.Vector.at_copy ponies ponyIndex))
    (let dx (+ (Std.copy msg.x) (Std.copy pony.x)))
    (let dy (+ (Std.copy msg.y) (Std.copy pony.y)))

    ;Are we in the map?
    (if
        (and
            (and
                (> dx -1)
                (< dx w)
            )
            (and
                (> dy -1)
                (< dy h)
            )
        )
        {
            ;(print "In map!")
            
            ;Is the obstacle on the checked field?
            (if
                (Std.String.eq
                    (getMapField map dx dy)
                    "#"
                )
                {
                    ;(print "Obstacle!")
                    -1
                }
                {
                    ;Is the reward on checked field?
                    (if
                        (Std.String.eq
                            (getMapField map dx dy)
                            "*"
                        )
                        {
                            ;(print "Reward!")
                            ;Is reward present?
                            (if
                                (=                                
                                    (findRewardByPosition rewards dx dy 0)
                                    1
                                )
                                {
                                    1
                                }
                                {
                                    ;Is there another pony?
                                    (if
                                        (> 
                                            (findPonyByPosition ponies dx dy 0)
                                            -1
                                        )
                                        {
                                            ;(print "Is there other pony!")
                                            -1
                                        }
                                        {
                                            ;(print "Clear!")
                                            0
                                        }
                                    )
                                }
                            )
                        }
                        {
                            ;Is there another pony?
                            (if
                                (> 
                                    (findPonyByPosition ponies dx dy 0)
                                    -1
                                )
                                {
                                    ;(print "Is there other pony!")
                                    -1
                                }
                                {
                                    ;(print "Clear!")
                                    0
                                }
                            )
                        }
                    )
                }
            )
        }
        {
            ;(print "Out of map!")
            -1
        }
    )
})

(let parseMessageFromPony (map w h ponies rewardCounter rewards) {
    (let msg (Std.Actor.receive))
    ;(print msg)
    ;(print map)
    ;(print w)
    ;(print h)
    ;(print ponies)
    ;(print rewardCounter)

    (let ponyIndex (findPonyByName ponies (Std.copy msg.name) 0))
    
    ;Does this pony exist?
    (if
        (> ponyIndex -1)
        {
            (if 
                (Std.String.eq (Std.copy msg.command) "check")
                (tailcall checkMapForPony msg map w h ponies rewardCounter rewards ponyIndex)
                (if
                    (Std.String.eq (Std.copy msg.command) "move")
                    (tailcall movePony msg map w h ponies rewardCounter rewards ponyIndex)
                    0 ;(tailcall parseMessageFromPony map w h ponies rewardCounter rewards)
                )
            )
        }
        0
    )
})

(let checkMapForPony (msg map w h ponies rewardCounter rewards ponyIndex) {
    (let status (checkFieldStatus msg map w h ponies rewardCounter rewards ponyIndex))
    (Std.Actor.send (Std.copy msg.addr) status)
    (parseMessageFromPony map w h ponies rewardCounter rewards)
})

(let movePony (msg map w h ponies rewardCounter rewards ponyIndex) {
    
    (let x (Std.copy msg.x))
    (let y (Std.copy msg.y))
    (let z 0)
    (let status (checkFieldStatus msg map w h ponies rewardCounter rewards ponyIndex))

    (let results 
    (if
        (or
            (or
                (and (= -1 x) (= 0 y))
                (and (= 1 x) (= 0 y))
            )
            (or
                (and (= -1 y) (= 0 x))
                (and (= 1 y) (= 0 x))
            )
        )
        {
            ;Moves are correct - let's check the status of field of dest
            (if
                (= status (+ z 1))
                {
                    ;We got the reward!
                    ;(print "Reward!")
                    (let rewards (takeReward rewards ponies ponyIndex x y))
                    (let rewardCounter (- rewardCounter 1))
                    (let ponies (updatePonyField ponies (vector) ponyIndex x y 1 0))
                    (printOutTheWorld map w h 0 ponies rewards)                    
                    (+ z 1)
                }
                {
                    (if
                        (= status 0)
                        {
                            ;No prise, no obstacle -lets move
                            ;(print "Move!")
                            (let ponies (updatePonyField ponies (vector) ponyIndex x y 0 0))
                            (printOutTheWorld map w h 0 ponies rewards)                            
                            z
                        }
                        {
                            ;Obstacle - cannot move
                            ;(print "Obstacle!")
                            (- z 1)
                        }
                    )
                }
            )
        }
        {
            ;Incorrect move
            (let status status)
            ;(print "Bad move!")
            (- z 1)
        }
    )

    )

    (Std.Actor.send (Std.copy msg.addr) results
    )

    
    
    (if
        (= rewardCounter z)
        {
            (print ponies)
            0
        }
        {
            
            (parseMessageFromPony map w h ponies rewardCounter rewards)
        }
        
    )
})

(let world (map w h appliedStrategies) {
    ;Found all declared ponies and all rewards
    (let ponies (extractPoniesInField map 0 0 w h (vector)))
    (let rewardCounter (countRewardsInField map 0 0 w h 0))
    (let rewards (extractRewardsInField map 0 0 w h (vector)))
    ;(print rewards)

    ;Run ponies regarding applied strategies
    (runPony ponies appliedStrategies (Std.Actor.self) 0)

    ;Render the first map
    (printOutTheWorld map w h 0 ponies rewards)

    ;Listen for messages from ponies
    (parseMessageFromPony map w h ponies rewardCounter rewards)
})

(let main () {
    (let map (vector))
    (Std.Vector.push map "   *")
    (Std.Vector.push map " ## ")
    (Std.Vector.push map "  # ")
    (Std.Vector.push map "a  Z")

    (let appliedStrategies (vector))
    (Std.Vector.push appliedStrategies "a:dijks")
    (Std.Vector.push appliedStrategies "Z:astar")

    (actor world map 4 4 appliedStrategies)
    0
})


